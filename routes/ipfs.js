var express = require('express');
var router = express.Router();
var ipfsAPI = require('ipfs-api')
var fs = require('fs');
var querystring = require('querystring');
var http = require('http');
var request = require('request');
var Web3 = require('web3');
var web3 = new Web3();
// var dataRegistryArtifact = require("../DataRegistry.json");
var dataRegistryArtifact = require("../DataRegistry-new.json");
var contract = require("truffle-contract");
var BigNumber = require('bignumber.js');
var bs58 = require('bs58');

function ByteArraytoHexString(byteArray) {
  return Array.from(byteArray, function(byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
  }).join('')
}

function fromIPFSHash(hash) {
  //ex: QmQakoQGAv4Hq6eaBoUNR1PpqHaL8REMRWoHkrpze1xSNA
   var bytes = bs58.decode(hash);
   var multiHashId = 2;
   // remove the multihash hash id
   var hexval = ByteArraytoHexString(bytes.slice(multiHashId, bytes.length));
   return new BigNumber("0x" + hexval);
}

function toIPFSHash(str) {
    // remove leading 0x
    const remove0x = str.slice(2, str.length);
    // add back the multihash id
    const bytes = Buffer.from(`1220${remove0x}`, "hex");
    const hash = bs58.encode(bytes);
    return hash;
}

var categories = ["Fingerprint", "Faceshot", "MissingPerson", "CriminalRecord", "TrafficAlert", "Uncategorized"];

// "QmQakoQGAv4Hq6eaBoUNR1PpqHaL8REMRWoHkrpze1xSNA" face
// "QmVu3jEW7FDATjbgupQUV3x4CCcagk4TdiDk74DnEjv6rf" fingerprint
// "QmWXvNRpHdvRyhpdWj91RJ5RmkfJNppK92EXjFf13ThHJ8" json
/* GET home page. */
router.get('/chain/:idx', function(req, res, next) {
    var idx = req.params.idx;

    var ipfs = ipfsAPI('localhost', '5001', {protocol: 'http'}) // leaving out the arguments will default to these values

    const TX_DEFAULTS = { from: "0x92a998e2d04497ad3a453aaf3da9b1e86e04bc67", gas: 8000000 };

    // var provider = new Web3.providers.HttpProvider("http:\/\/localhost:8544");
    var provider = new Web3.providers.HttpProvider("http://localhost:8544");
    var web3 = new Web3(provider);


    var dataRegistry = contract(dataRegistryArtifact)
    dataRegistry.setProvider(provider);
    dataRegistry.setNetwork('22392');

    // var top = await dataRegistry.deployed().then((instance) => {
    //     return instance.getTop.call();
    //     //return instance.getData.call(new BigNumber(idx));
    // });

    dataRegistry.deployed().then(instance => {
      console.log("inside dataregistry deployed");
      // console.log("instance = ", instance);

      instance.getTop.call().then(top => {
                console.log("top = ", top); 
         return top;
      }, error => {
        console.error("top error = ", error);
      }).then(top => {
          return instance.getData.call('0x1');
      }, error => {
        console.error("getData error = ", error);
      }).then(results => {
        console.log(results);
	console.log(results[0].toString(16));
        var ipfs_address = "QmSjjaiNtY4ijKzp5zGR351hQZVyHzxjroRftf5EoaW2ii";//toIPFSHash("0x" + results[0].toString(16));
	console.log("ipfs_address", ipfs_address);
        ipfs.get(ipfs_address, async (err, files) => {
         if (err) {
           console.error("err = ", err);
         }
         files.forEach(function(file){
           console.log(file.path);
           var src = file.content;
           console.log("src");
           console.log(src);
           var temp = JSON.stringify(src.toString());

            // var temp = JSON.parse(JSON.stringify(src.toString()));
            // var temp = JSON.parse(src.toString());
            console.log("temp");
           console.log(temp);

           console.log({ "hi": "bye" })
           //returns ipfs, creationts, reportts, reporter, complete, category
           var jsonpost = {"uploader": results[3],
                     "unix_date": results[1].toString(10),
                     "ipfs": ipfs_address,
                     "category": results[5].toString(10),
                     "mimetype": results[6],
                     "data": file};

        request.post({
          url: 'https://search-dynamo-hackathon-luhe6v5vah7nd5nrswov76xwue.us-east-1.es.amazonaws.com/missing_person/json/1',
          method: "POST",
          // json: temp
          // json: true,
          json: { body: temp }
          // body: temp,
                     
            // request.post({
            //   url: 'https://search-dynamo-hackathon-luhe6v5vah7nd5nrswov76xwue.us-east-1.es.amazonaws.com/missing_person/json/1',
            //   method: "POST",
            //   // json: temp
            //   // json: true,
            //   json: jsonpost
            //   // body: temp,
            }, function (error, response, body) {

              console.log("anything");
              // console.log("response = ", response);
              if (error) {
                console.error(error);
              }
              if (!error && response.statusCode === 200) {
                console.log(body);
              }
            });
         });
        });
      });
    }, (error) => {
      console.log("dataregistry error = ", error);
    });


    // console.log("top is", top);
    // res.json({ top: top });


       // get an image
    // ipfs.get('/ipfs/QmVu3jEW7FDATjbgupQUV3x4CCcagk4TdiDk74DnEjv6rf', function(err, files) {
    //  if (err) {
    //    console.error("err = ", err);
    //  }
    //  files.forEach(function(file){
    //    console.log(file.path);
    //    const src = file.content;
    //    console.log(src);
    //    var all = fs.createWriteStream("out.png");
      // all.write(src);
    //  })
    // })

    // ipfs.get('/ipfs/QmWXvNRpHdvRyhpdWj91RJ5RmkfJNppK92EXjFf13ThHJ8', async (err, files) => {
    //  if (err) {
    //    console.error("err = ", err);
    //  }
    //  files.forEach(function(file){
    //    console.log(file.path);
    //    var src = file.content;
    //    console.log("src");
    //    console.log(src);
        // var temp = JSON.parse(JSON.stringify(src.toString()));
    //     // var temp = JSON.parse(src.toString());
    //     console.log("temp");
    //    console.log(temp);

    //    console.log({ "hi": "bye" })

        // request.post({
        //   url: 'https://search-dynamo-hackathon-luhe6v5vah7nd5nrswov76xwue.us-east-1.es.amazonaws.com/test_missing_person/json/person',
        //   method: "POST",
        //   // json: temp
        //   // json: true,
        //   json: { body: temp }
        //   // body: temp,
    //     }, function (error, response, body) {

    //       console.log("anything");
    //       // console.log("response = ", response);
    //       if (error) {
    //         console.error(error);
    //       }
    //       if (!error && response.statusCode === 200) {
    //         console.log(body);
    //       }
    //     })
    //  })
    // })



  res.render('index', { title: 'ipfs' });
});

module.exports = router;
